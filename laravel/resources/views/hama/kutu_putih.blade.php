<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{asset('assets/CSS_files/for-each-image.css')}}">
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
  <link rel="stylesheet" type="text/css" href="Plugins/slick-master/slick/slick-theme1.css" />
  <script type="text/javascript" src="Plugins/slick-master/slick/slick.min.js"></script>
  <script src="{{asset('assets/JS_files/for-each-image.js')}}"></script>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="Plugins\jQuery-Plugin-For-Image-Hover-Zoom-WM-Zoom\wm-zoom\jquery.wm-zoom-1.0.min.css">
  <style>
    a:hover,
    a {
      color: black
    }
  </style>
</head>

<body>
  <!--header part start-->
  <div class="container-fluid text-white" id="change-color">
    <div class="row" id="top-containt">
      <div class="col-4 text-center">
        <a href="ecommerce.html"><img src="{{asset('assets/Images/roroicon.png')}}" height="130px" class="color logo-1 logo-sift"></a>
        <a href="ecommerce.html"><img src="{{asset('assets/Images/pinterest_icon.png')}}" height="70px" class=" color logo-2 mt-1 ml-2"></a>
      </div>
      <div class="col-8 pt-4 mt-1 text-center">
        <span class="dropdown dropdown-toggle profile" data-toggle="dropdown" style="color:black">
          <a href="#/"><img src="{{asset('assets/Images/profile-icon.png')}}" class="profile-icon color"></a>
        </span>
        <ul class="dropdown-menu mt-4 text-center" style="margin-left:-6%;border-radius:10px;">
          <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Help Center </a></li>
          <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Order History</a></li>
          <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Change Password</a>
          </li>
          <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Update Profile</a></li>
          <li class="pre-profile-1 pt-2 pb-2"><a href="login-form.php" class="color" style="font-weight:600">Log In</a>
          </li>
        </ul>
        <span class="menu-1"><a href="home" class="color">home</a></span>
        <span class="menu"><a href="product" class="color">P and P</a></span>
        <span class="menu"><a href="about_us" class="color">about</a></span>
        <span class="menu"><a href="share_info" class="color">share info</a></span>
        <span class="menu-bar text-right"><a href="#/" class="color">&#9776;</a></span>
        <span class="menu-bar-1 text-right"><a href="#/" class="color">&#9776;</a></span>
        <span class="menu-bar-2 text-right"><a href="#/" class="color">&#9776;</a></span>
      </div>
    </div>
    <div class="container-fluid side-bar px-0">
      <div class="col-12 text-right bg-danger">
        <span class="close"><a href="#/" class="color">&times;</a></span>
      </div>
      <ul class="pt-5 pre-side-bar">
        <li class="pt-4" style="margin-left:-5%;"><a href="home" class="color">home</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="product" class="color">P and P</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="about_us" class="color">about</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="share_info" class="color">share info</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="#/" class="color">Update profile</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="login-form.php" class="color">Log In</a><span><a href="#"
              class="color"><img src="{{asset('assets/Images/log-in..png')}}" class="log-in"></a></span></li>
      </ul>
    </div>
  </div>
  <!--header part end-->
  <!--middle part start-->
  <div class="container mt-5 pt-4 hide-for-tablate">
    <div class="row pt-5">
      <div class="col-1">
        <div class="row pt-3 pb-2">
          <div class="col-xl-12 text-center">
            <a href="#/"><img src="{{asset('assets/Images/hama1.jpeg')}}" height="50px" width="100%" class="zooming"></a>
          </div>
          <div class="col-xl-12 mt-3">
            <a href="#/"><img src="{{asset('assets/Images/hama2.jpeg')}}" height="50px" width="100%" class="zooming-1"></a>
          </div>
          <div class="col-xl-12 mt-3 front-show">
            <a href="#/"><img src="{{asset('assets/Images/hama3.jpeg')}}" height="55px" width="75%" class="zooming-2"></a>
            <a href="#/">
              <div style="font-size:25px;z-index:1;color:black;position: absolute;padding-left:20%;"
                class="pt-1 front-show">+3</div>
            </a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama3.jpeg')}}" height="50px" width="100%" class="zooming-5"></a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama4.jpg')}}" height="50px" width="100%" class="zooming-3"></a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama5.jpg')}}" height="50px" width="100%" class="zooming-4"></a>
          </div>
        </div>
      </div>
      <div class="col-5 text-center zooming-open-head wm-zoom-container my-zoom-1">
        <div class="wm-zoom-box">
          <img src="{{asset('assets/Images/hama1.jpeg')}}" height="400px" width="405px" class="wm-zoom-default-img" alt="alternative text"
            data-hight-src="hama1.jpeg">
        </div>
      </div>
      <!--hidden-->
      <div class="col-5 text-center zooming-open-1 wm-zoom-container my-zoom-1">
        <div class="wm-zoom-box">
          <img src="{{asset('assets/Images/hama2.jpeg')}}" height="400px" width="405px" class="wm-zoom-default-img" alt="alternative text"
            data-hight-src="hama2.jpeg">
        </div>
      </div>
      <!--hidden-->
      <div class="col-5 text-center zooming-open-2">
        <img src="{{asset('assets/Images/hama3.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-5 text-center zooming-open-3">
        <img src="{{asset('assets/Images/hama4.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-5 text-center zooming-open-4">
        <img src="{{asset('assets/Images/hama5.jpeg')}}" height="400px" width="95%">
      </div>
      <div class="col-5">
        <div class="row">
          <div class="col-12 pt-3">
            <p style="font-size:25px;font-weight:600">Kutu Putih</p>
          </div>
          <div class="col-12 pl-5">
            <p style="font-weight:600;font-size:20px;"> Jenis : <span style="color:red">Hama Tanaman</span></p>
          </div>
          <div class="col-12">
            <ul>
              <li>Kutu putih adalah sejenis serangga skala</li>
              <li>Serangga berbentuk oval bertubuh lunak ini tampak seperti serpihan kecil serat pengering berwarna
                putih.</li>
              <li>Kutu putih suka menyedot kehidupan dari tanaman dan memakan sarinya.</li>
              <li>Biasanya koloni kecil ini berada di sisi bawah daun, di sekitar pertumbuhan baru, dan di celah kecil
                antara daun dan batang.</li>
              <li>Cara membasminya:</li>
              <li>Kutu putih menyebar dengan cepat, sehingga sulit dikendalikan. Anda bisa mulai dengan mencelupkan
                kapas ke dalam alkohol dan mengoleskan setiap kutu putih yang Anda lihat. Bisa juga dengan bantuan
                produk pembasmi hama jika ingin lebih praktis.</li>
              <li>Untuk info lebih lanjut terkait cara pembasmian hama ini bisa klik tombol "Eksplore" di bawah.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row pt-2">
      <div class="col-6">
        <span style="visibility: hidden;;">hello user</span>
      </div>
      <div class="col-6 pl-5 text-white">
        <a href="https://www.youtube.com/watch?v=sEEzFRgF7ck" style="color:white"><span
            style="width:30%;border:1px solid;padding:2% 5%;text-transform: uppercase;font-size:15px;font-weight:600;background-color:rgb(255, 174, 0);border-radius:10px;"><i
              aria-hidden="true"></i><span class="ml-2">Eksplore</span></span></a>
      </div>
    </div>
    <div class="rows pb-5">
      <div class="col">
        <span style="visibility:hidden;">hello user</span>
      </div>
    </div>
  </div>
  <div class="container show-for-small-devices mt-5 pt-1">
    <div class="row pt-5 mt-5">
      <div class="col-2 col-xs-3">
        <div class="row pt-3 pb-2">
          <div class="col-xl-12 text-center">
            <a href="#/"><img src="{{asset('assets/Images/hama1.jpeg')}}" class="zooming"></a>
          </div>
          <div class="col-xl-12 mt-3">
            <a href="#/"><img src="{{asset('assets/Images/hama2.jpeg')}}" class="zooming-1"></a>
          </div>
          <div class="col-xl-12 mt-3 front-show">
            <a href="#/"><img src="{{asset('assets/Images/hama3.jpeg')}}" height="55px" width="75%" class="zooming-2"></a>
            <a href="#/">
              <div class="pt-2 front-show block-img-for-tablate">+3</div>
            </a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama3.jpeg')}}" class="zooming-5"></a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama4.jpeg')}}" class="zooming-3"></a>
          </div>
          <div class="col-xl-12 mt-3 front-hide">
            <a href="#/"><img src="{{asset('assets/Images/hama5.jpeg')}}" class="zooming-4"></a>
          </div>
        </div>
      </div>
      <div class="col-10 col-xs-9 text-center zooming-open-head ">
        <img src="{{asset('assets/Images/hama1.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-10 text-center zooming-open-1">
        <img src="{{asset('assets/Images/hama2.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-10 text-center zooming-open-2">
        <img src="{{asset('assets/Images/hama3.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-10 text-center zooming-open-3">
        <img src="{{asset('assets/Images/hama4.jpeg')}}" height="400px" width="95%">
      </div>
      <!--hidden-->
      <div class="col-10 text-center zooming-open-4">
        <img src="{{asset('assets/Images/hama5.jpeg')}}" height="400px" width="95%">
      </div>
    </div>
    <div class="row mt-5 pt-5">
      <div class="col-12 pt-3">
        <p style="font-size:25px;font-weight:600">Kutu Putih</p>
      </div>
      <div class="col-12 pl-5">
        <p style="font-weight:600;font-size:20px;"> Jenis : <span style="color:red">Hama Tanaman</span></p>
      </div>
      <div class="col-12">
        <ul>
          <li>Kutu putih adalah sejenis serangga skala</li>
          <li>Serangga berbentuk oval bertubuh lunak ini tampak seperti serpihan kecil serat pengering berwarna putih.
          </li>
          <li>Kutu putih suka menyedot kehidupan dari tanaman dan memakan sarinya.</li>
          <li>Biasanya koloni kecil ini berada di sisi bawah daun, di sekitar pertumbuhan baru, dan di celah kecil
            antara daun dan batang.</li>
          <li>Cara membasminya:</li>
          <li>Kutu putih menyebar dengan cepat, sehingga sulit dikendalikan. Anda bisa mulai dengan mencelupkan kapas ke
            dalam alkohol dan mengoleskan setiap kutu putih yang Anda lihat. Bisa juga dengan bantuan produk pembasmi
            hama jika ingin lebih praktis.</li>
          <li>Untuk info lebih lanjut terkait cara pembasmian hama ini bisa klik tombol "Eksplore" di bawah.</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="#/" style="color:white"><span
            style="width:30%;border:1px solid;padding:2% 5%;text-transform: uppercase;font-size:15px;font-weight:600;background-color:rgb(255, 174, 0);border-radius:10px;"><i
              aria-hidden="true"></i><span class="ml-2">Explore</span></span></a><br class="break-1"><br
          class="break-1">
      </div>
    </div>
    <div class="container-fluid mt-5">
      <span style="visibility: hidden;">hi user</span>
    </div>
  </div>
  <!--middle part end-->

  
  <!--footer-->
  <div class="container-fluid bg-dark text-white">
    <div class="container">
      <div class="row ">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5">
          <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-right">Roro Jonggrang
          </p>
          <p data-aos="slide-right">Web Rorojonggrang<br>
            dibuat sejak tahun 2021.<br>
            Web tentang tanaman hias dan,<br>
            hama serta menjual produk pembasmi hama.<br></p>
        </div>
        <div class="col-xl-3 pt-5 hide-girl">
          <span style="font-size:20px;text-transform: uppercase;font-weight: 800;"
            data-aos="zoom-in-left">P and P</span><br>
          <p style="margin-top:3%" data-aos="slide-up">Tanaman Hias</p>
          <p style="margin-top:-3%" data-aos="slide-up">Jenis Hewan</p>
          <p style="margin-top:-3%" data-aos="slide-up">Pembasmi Hama</p>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 footer  pt-5">
          <span style="font-size:20px;text-transform: uppercase;font-weight: 800;" class="text-center"
            data-aos="zoom-in-left">Social Media </span><br>
          <p style="margin-top:3%;margin-left:1%;" data-aos="fade-up">@noratrivetisia</p>
          <p style="margin-top:-3%;margin-left:1%;" data-aos="fade-up">@Triwulandari</p>
          <p style="margin-top:-3%;margin-left:1%;" data-aos="fade-up">@NawangAnggita</p>
        </div>
        <div class="col-xl-2 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5 pb-3">
          <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-left">Contact</p>
          <p data-aos="fade-up"><i class="fa fa-home" aria-hidden="true" style="font-size:20px;"></i>&nbsp; Purwokerto,
            53147,</p>
          <p data-aos="fade-up"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; rorojonggrang@gmail.com</p>
          <p data-aos="fade-up"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +62 876 9875 0123</p>
        </div>
      </div>
    </div>
    <hr style="color:white;width:100%;border:1px solid">
    <div class="container mt-4 pb-4">
      <div class="row">
        <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 ">
          <p>© 2020 Copyright:<span style="font-weight:600">Rorojonggrang</span></p>
        </div>
        <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 icon-head">
          <span class="icons-2"><i class="fa fa-facebook" aria-hidden="true"></i></span>
          <span class="icons-3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
          <span class="icons-1"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
          <span class="icons"><i class="fa fa-instagram" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->
  <script type="text/javascript">
    //for single slider
    $('.single-slider').slick({
      dots: false,
      infinite: false,
      autoplaySpeed: 300,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: false
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            arrows: false
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            arrows: false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  </script>
  <script type="text/javascript"
    src="Plugins\jQuery-Plugin-For-Image-Hover-Zoom-WM-Zoom\assets\js\jquery-1.11.1.js"></script>
  <script type="text/javascript"
    src="Plugins\jQuery-Plugin-For-Image-Hover-Zoom-WM-Zoom\wm-zoom\jquery.wm-zoom-1.0.min.js"></script>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('.my-zoom-1').WMZoom();
      config: {
        inner: true
      }
    });
  </script>
  <script>
    AOS.init({
      once: true,
      duration: 1000,
    });
  </script>

</body>

</html>