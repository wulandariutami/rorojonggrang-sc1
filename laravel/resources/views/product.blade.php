
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{asset('assets/CSS_files/ecommerce-price-men.css')}}">
  <script src="{{asset('assets/JS_files/ecommerce-price-men.js')}}"></script>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
  <link rel="stylesheet" type="text/css" href="Plugins/slick-master/slick/slick-theme1.css" />
  <script type="text/javascript" src="Plugins/slick-master/slick/slick.min.js"></script>
  <script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js"></script>
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.css" />
  <style>
    a {
      color: black
    }

    a:hover {
      color: black
    }
  </style>
</head>

<body>

  <!--header part start-->
  <div class="container-fluid text-white" id="change-color">
    <div class="row" id="top-containt">
      <div class="col-4 text-center">
        <a href="home"><img src="{{asset('assets/Images/roroicon.png')}}" height="130px"
            class="color logo-1 logo-sift"></a>
        <a href="home"><img src="{{asset('assets/Images/pinterest_icon.png')}}" height="70px"
            class=" color logo-2 mt-1 ml-2"></a>
      </div>
      <div class="col-8 pt-4 mt-1 text-center">
        <span class="dropdown dropdown-toggle profile" data-toggle="dropdown" style="color:black">
          <a href="#/"><img src="{{asset('assets/Images/profile-icon.png')}}" class="profile-icon color"></a>
        </span>
        <ul class="dropdown-menu mt-4 text-center" style="margin-left:-6%;border-radius:10px;">
          @if(auth()->check())
            <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Help Center </a></li>
            <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Order History</a></li>
            <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Change Password</a></li>
            <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Update Profile</a></li>
            <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.logout') }}" class="color" style="font-weight:600">Log Out</a></li>
          @else
            <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.login') }}" class="color" style="font-weight:600">Log In </a></li>
            <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.register') }}" class="color" style="font-weight:600">Register</a></li>
          @endif
        </ul>
        <span class="menu-1"><a href="home" class="color">home</a></span>
        <span class="menu"><a href="product" class="color">P and P</a></span>
        <span class="menu"><a href="about_us" class="color">about</a></span>
        <span class="menu"><a href="share_info" class="color">share info</a></span>
        <span class="menu-bar text-right"><a href="#/" class="color">&#9776;</a></span>
        <span class="menu-bar-1 text-right"><a href="#/" class="color">&#9776;</a></span>
        <span class="menu-bar-2 text-right"><a href="#/" class="color">&#9776;</a></span>
      </div>
    </div>
    <div class="container-fluid side-bar px-0">
      <div class="col-12 text-right bg-danger">
        <span class="close"><a href="#/" class="color">&times;</a></span>
      </div>
      <ul class="pt-5 pre-side-bar">
        <li class="pt-4" style="margin-left:-5%;"><a href="home" class="color">home</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="product" class="color">P and P</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="about_us" class="color">about</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="share_info" class="color">share info</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="#/" class="color">Update profile</a></li>
        <li class="pt-3" style="margin-left:-5%"><a href="ecommerce.html" class="color">log out</a><span><a href="#"
              class="color"><img src="{{asset('assets/Images/log-in..png')}}" class="log-in"></a></span></li>
      </ul>
    </div>
  </div>
  <!--header part end-->
  <!--middle-->
  <div class="container-fluid mt-5 pt-5">
    <div class="row">

      <!--left side col start-->
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
        <div class="container large-left-sider">
          <div class="row">
            <div class="col-12 px-0 pt-5 pb-4 text-center pl-0">
              <input class="search-box" type="text" placeholder="Search.."><span><button class="search-button"><i
                    class="fa fa-search" id="search-icon"></i></button></span>
            </div>
          </div>
          <div class="row pl-3">
            <div class="col-12 pt-3">
              <p style="font-size:20px;text-transform: uppercase;font-weight: 600;">Harga Pembasmi Hama</p>
              <span class="text-bold" name="select-price"><input type="radio" name="price"
                  class="open-content-one-hundred"> &nbsp;Rp.10.000-15.000</span><br>
              <span class="text-bold" name="select-price"><input type="radio" name="price"
                  class="open-content-two-hundred"> &nbsp;Rp.15.000-20.000</span><br>
              <span class="text-bold" name="select-price"><input type="radio" name="price"
                  class="open-content-three-hundred"> &nbsp;Rp.20.000-25.000</span><br>
              <span class="text-bold" name="select-price"><input type="radio" name="price"
                  class="open-content-four-hundred"> &nbsp;Rp.25.000-30.000</span><br>
              <span class="text-bold" name="select-price"><input type="radio" name="price"
                  class="open-content-five-hundred"> &nbsp;Rp.30.000-35.000</span><br>
            </div>
            <div class=" col-12 pt-5">
              <p style="font-size:20px;text-transform: uppercase;font-weight: 600;">Jenis Tanaman</p>
              <span class="text-bold"><input type="radio" name="color" class="open-content-three-hundred"> &nbsp;Janda Bolong</span><br>
              <span class="text-bold"><input type="radio" name="color" class="open-content-five-hundred"> &nbsp;Lili Paris</span><br>
              <span class="text-bold"><input type="radio" name="color" class="open-content-two-hundred"> &nbsp; Simbar Menjangan</span><br>
              <span class="text-bold"><input type="radio" name="color" class="open-content-four-hundred"> &nbsp;Karet Kebo</span><br>
            </div>
            <div class=" col-12 pb-5 pt-5">
              <p style="font-size:20px;text-transform: uppercase;font-weight: 600;">Jenis Hama</p>
              <span class="text-bold"><input type="radio" name="brand" class="open-content-two-hundred"> &nbsp;Kutu
                Putih</span><br>
              <span class="text-bold"><input type="radio" name="brand" class="open-content-three-hundred"> &nbsp;Tungau
                laba -laba</span><br>
              <span class="text-bold"><input type="radio" name="brand" class="open-content-four-hundred"> &nbsp; Ulat
                Bulu</span><br>
              <span class="text-bold"><input type="radio" name="brand" class="open-content-five-hundred"> &nbsp;Wereng
                Coklat</span><br>
            </div>
          </div>
        </div>
      </div>
      <!--left side col end-->
      <!--right side col start-->

      <!--front look-->
      <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 pl-0" style="border-left:1px solid grey">
        <div class="container large-right-sider">
          <div class="row pt-5 hide-front-page">
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="simbar_menjangan"><img src="{{asset('assets/Images/coba4.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"></a>
              <a href="simbar_menjangan"><span class="image-lable" data-aos="fade-up">Simbar
                  Menjangan</span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="kebo_karet"><img src="{{asset('assets/Images/coba5.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"><br></a>
              <a href="kebo_karet"><span class="image-lable" data-aos="fade-up">Kebo
                  Karet</span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 text-center hide-containt-tablate">
              <a href="janda_bolong"><img src="{{asset('assets/Images/coba6.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"></a>
              <a href="janda_bolong"><span class="image-lable" data-aos="fade-up">Janda
                  Bolong</span></a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 text-center hide-containt-small-device">
              <a href="lili_paris"><img src="{{asset('assets/Images/coba7.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"><br></a>
              <a href="lili_paris"><span class="image-lable" data-aos="fade-up">Lili
                  Paris</span></a>
            </div>
          </div>

          <!--second-->
          <div class="row pt-5 hide-front-page">
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="kutu_putih"><img src="{{asset('assets/Images/hama1.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"></a>
              <a href="kutu_putih"><span class="image-lable" data-aos="fade-up">Kutu
                  Putih</span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="ulat_bulu"><img src="{{asset('assets/Images/hama3.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="height:180px"><br></a>
              <a href="ulat_bulu"><span class="image-lable" data-aos="fade-up">Ulat
                  Bulu</span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 text-center hide-containt-tablate">
              <a href="wareng_coklat"><img src="{{asset('assets/Images/hama10.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="border-radius:20px;height:180px"></a>
              <a href="wareng_coklat"><span class="image-lable" data-aos="fade-up">Wereng
                  Coklat</span></a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 text-center hide-containt-small-device">
              <a href="fungus_gnats"><img src="{{asset('assets/Images/hama9.jpeg')}}" class="img-men" data-aos="fade-up"
                  style="height:180px"><br></a>
              <a href="fungus_gnats"><span class="image-lable" data-aos="fade-up">Fungus
                  Gnats</span></a>
            </div>
          </div>

          <!--third-->
          <br>
          <div class="col text-center">
          <span style="font-size:30px;text-transform:uppercase;font-weight:bold" data-aos="zoom-in">PRODUCT</span>
        </div>
          <div class="row pt-5 hide-front-page">
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="baygon"><img src="{{asset('assets/Images/for-each-image-1now.jpeg')}}" class="img-men"
                  data-aos="fade-up" style="border-radius:20px;height:180px"></a>
              <a href="baygon"><span class="image-lable" data-aos="fade-up">Baygon
                </span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6  text-center">
              <a href="insektisida"><img src="{{asset('assets/Images/produk2.jpeg')}}" class="img-men"
                  data-aos="fade-up" style="border-radius:20px;height:180px"><br></a>
              <a href="insektisida"><span class="image-lable" data-aos="fade-up">Insektisida
                </span></a>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4 text-center hide-containt-tablate">
              <a href="cklt"><img src="{{asset('assets/Images/produk4.jpeg')}}" class="img-men"
                  data-aos="fade-up" style="border-radius:20px;height:180px"></a>
              <a href="cklt"><span class="image-lable" data-aos="fade-up">CK-L T ANT
                </span></a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 text-center hide-containt-small-device">
              <a href="ralat"><img src="{{asset('assets/Images/produk6.jpeg')}}" class="img-men"
                  data-aos="fade-up" style="border-radius:20px;height:180px"></a>
              <a href="ralat"><span class="image-lable" data-aos="fade-up">Ralat
                </span></a>
            </div>
          </div>
           <!--tambahan-->

        
          <!--footer-->
          <div class="container-fluid bg-dark text-white">
            <div class="container">
              <div class="row ">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5">
                  <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-right">Roro
                    Jonggrang</p>
                  <p data-aos="slide-right">Web Rorojonggrang<br>
                    dibuat sejak tahun 2021.<br>
                    Web tentang tanaman hias dan,<br>
                    hama serta menjual produk pembasmi hama.<br></p>
                </div>
                <div class="col-xl-3 pt-5 hide-girl">
                  <span style="font-size:20px;text-transform: uppercase;font-weight: 800;"
                    data-aos="zoom-in-left">P and P</span><br>
                  <p style="margin-top:3%" data-aos="slide-up">Tanaman Hias</p>
                  <p style="margin-top:-3%" data-aos="slide-up">Jenis Hewan</p>
                  <p style="margin-top:-3%" data-aos="slide-up">Pembasmi Hama</p>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 footer  pt-5">
                  <span style="font-size:20px;text-transform: uppercase;font-weight: 800;" class="text-center"
                    data-aos="zoom-in-left">Social Media </span><br>
                  <p style="margin-top:3%;margin-left:1%;" data-aos="fade-up">@noratrivetisia</p>
                  <p style="margin-top:-3%;margin-left:1%;" data-aos="fade-up">@Triwulandari</p>
                  <p style="margin-top:-3%;margin-left:1%;" data-aos="fade-up">@NawangAnggita</p>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5 pb-3">
                  <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-left">Contact
                  </p>
                  <p data-aos="fade-up"><i class="fa fa-home" aria-hidden="true" style="font-size:20px;"></i>&nbsp;
                    Purwokerto, 53147,</p>
                  <p data-aos="fade-up"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; rorojonggrang@gmail.com
                  </p>
                  <p data-aos="fade-up"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +62 876 9875 0123</p>
                </div>
              </div>
            </div>
            <hr style="color:white;width:100%;border:1px solid">
            <div class="container mt-4 pb-4">
              <div class="row">
                <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 ">
                  <p>© 2020 Copyright:<span style="font-weight:600">Rorojonggrang</span></p>
                </div>
                <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 icon-head">
                  <span class="icons-2"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                  <span class="icons-3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                  <span class="icons-1"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                  <span class="icons"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                </div>
              </div>
            </div>
          </div>
          <!--footer-->
          <script type="text/javascript">
    //for single slider
    $('.single-slider').slick({
      dots: false,
      infinite: false,
      autoplaySpeed: 300,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: false
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            arrows: false
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            arrows: false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  </script>
  <script type="text/javascript"
    src="Plugins\jQuery-Plugin-For-Image-Hover-Zoom-WM-Zoom\assets\js\jquery-1.11.1.js"></script>
  <script type="text/javascript"
    src="Plugins\jQuery-Plugin-For-Image-Hover-Zoom-WM-Zoom\wm-zoom\jquery.wm-zoom-1.0.min.js"></script>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('.my-zoom-1').WMZoom();
      config: {
        inner: true
      }
    });
  </script>
  <script>
    AOS.init({
      once: true,
      duration: 1000,
    });
  </script>

</body>

</html>
</body>

</html>