@extends('admin.layout.app')
@section('header')
    @include('admin.layout.header')
@endsection
@section('leftbar')
    @include('admin.layout.leftbar')
@endsection
@section('rightbar')
    @include('admin.layout.rightbar')
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        {{ $module_name }}
        <small>{{ $submodule_name }}</small>
        </h1>
      <ol class="breadcrumb">
        <li>{{ $module_name }}</li>
        <li class="active">{{ $submodule_name }}</li>
      </ol>
    </section>

    <section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data</h3>
        </div>
        <div class="box-body">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                    </div>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection