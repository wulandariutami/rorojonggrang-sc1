<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="@if($module_name == 'Dashboard') active @endif">
          <a href="{{ route('admin.index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="@if($module_name == 'User') active @endif">
          <a href="{{ route('admin.user.index') }}">
            <i class="fa fa-user"></i> <span>User</span>
          </a>
        </li>
        <li class="@if($module_name == 'Tanaman') active @endif">
          <a href="{{ route('admin.tanaman.index') }}">
            <i class="fa fa-tree"></i> <span>Tanaman</span>
          </a>
        </li>
        <li class="@if($module_name == 'Hama') active @endif">
          <a href="{{ route('admin.hama.index') }}">
            <i class="fa fa-plus"></i> <span>Hama</span>
          </a>
        </li>
        <li class="@if($module_name == 'Product') active @endif">
          <a href="{{ route('admin.product.index') }}">
            <i class="ion ion-bag"></i> <span>Product</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
