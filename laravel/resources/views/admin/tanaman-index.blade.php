@extends('admin.layout.app')
@section('header')
    @include('admin.layout.header')
@endsection
@section('leftbar')
    @include('admin.layout.leftbar')
@endsection
@section('rightbar')
    @include('admin.layout.rightbar')
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $module_name }}
      </h1>
      <ol class="breadcrumb">
        <li class="active">{{ $module_name }}</li>
      </ol>
    </section>

    <section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <a href="{{ route('admin.tanaman.create') }}" class="btn btn-primary">Tambah Data</a>
        </div>
        <div class="box-body">
          <table class="table">
              <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Jenis</th>
                  <th>Desc</th>
                  <th>Image</th>
                  <th>Link</th>
                  <th>Action</th>
              </tr>
              @foreach($tanamans as $item)
              <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->jenis}}</td>
                <td>{{$item->desc}}</td>
                <td><img src="{{asset($item->image)}}" alt="image" height="250px"></td>
                <td>{{$item->link}}</td>
                  <td><a href="{{route('admin.tanaman.edit', ['tanaman'=>$item->id])}}" class="btn btn-warning">Edit</a> <a class="btn btn-danger" onclick="event.preventDefault();$('#tanamanDelete').submit()">Delete</a></td>
                  <form id="tanamanDelete" action="{{ route('admin.tanaman.destroy', ['tanaman'=>$item->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                  </form>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection