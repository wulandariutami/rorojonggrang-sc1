@extends('admin.layout.app')
@section('header')
    @include('admin.layout.header')
@endsection
@section('leftbar')
    @include('admin.layout.leftbar')
@endsection
@section('rightbar')
    @include('admin.layout.rightbar')
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        {{ $module_name }}
        <small>{{ $submodule_name }}</small>
        </h1>
      <ol class="breadcrumb">
        <li>{{ $module_name }}</li>
        <li class="active">{{ $submodule_name }}</li>
      </ol>
    </section>

    <section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data</h3>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.tanaman.update', ['tanaman'=>$tanaman->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name ="name" placeholder="Enter name" value="{{$tanaman->name}}">
                </div>
                <div class="form-group">
                    <label for="jenis">Jenis</label>
                    <input type="text" class="form-control" id="jenis" placeholder="jenis" name="jenis" value="{{$tanaman->jenis}}">
                </div>
                <div class="form-group">
                    <label for="desc">Desc</label>
                    <input type="text" class="form-control" id="desc" name ="desc"  placeholder="desc" value="{{$tanaman->desc}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                        <img src="{{asset($tanaman->image)}}" alt="image" height="250px">
                    </div>
                </div>
                <div class="form-group">
                    <label for="link">Link</label>
                    <input type="text" class="form-control" id="link" name ="link"  placeholder="link" value="{{$tanaman->link}}">
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection