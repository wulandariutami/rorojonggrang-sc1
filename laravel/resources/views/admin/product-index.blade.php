@extends('admin.layout.app')
@section('header')
    @include('admin.layout.header')
@endsection
@section('leftbar')
    @include('admin.layout.leftbar')
@endsection
@section('rightbar')
    @include('admin.layout.rightbar')
@endsection
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $module_name }}
      </h1>
      <ol class="breadcrumb">
        <li class="active">{{ $module_name }}</li>
      </ol>
    </section>

    <section class="content">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <a href="{{ route('admin.product.create') }}" class="btn btn-primary">Tambah Data</a>
        </div>
        <div class="box-body">
          <table class="table">
              <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Desc</th>
                  <th>Action</th>
              </tr>
              <tr>
                  <td>1</td>
                  <td>Caca</td>
                  <td>caca</td>
                  <td><a href="" class="btn btn-warning">Edit</a> <a href="" class="btn btn-danger">Delete</a></td>
              </tr>
          </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection