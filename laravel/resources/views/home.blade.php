<?php 
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
}

?>
 
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{asset('assets/CSS_files/ecommerce.css')}}">
        <script src="{{asset('assets/Js_files/ecommerce.js')}}"></script>
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="Plugins/slick-master/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="Plugins/slick-master/slick/slick-theme.css"/>
        <script type="text/javascript" src="Plugins/slick-master/slick/slick.min.js"></script>
        <style>
          .hide-slider{display:none}
          @media screen and (max-width:991px){
            .hide-slider{display:block}
          }
        </style>
    </head>
    <body>
       <!-- <div class="carousel slide" data-ride="carousel" style="z-index:1">-->
            <div class="container-fluid text-white" id="change-color">
              <div class="row" id="top-containt">
                <div class="col-4 text-center">
                  <a href="home"><img src="{{asset('assets/Images/roroicon.png')}}" height="130px" class="color logo-1 logo-sift"></a>
                  <a href="home"><img src="{{asset('assets/Images/pinterest_icon.png')}}" height="70px" class=" color logo-2 mt-1 ml-2"></a>
                </div>
                <div class="col-8 pt-4 mt-1 text-center">
                  <span class="dropdown dropdown-toggle profile" data-toggle="dropdown" style="color:black" >
                    <a href="#/"><img src="{{asset('assets/Images/profile-icon.png')}}" class="profile-icon color"></a>
                  </span>
                  <ul class="dropdown-menu mt-4 text-center" style="margin-left:-6%;border-radius:10px;">
                    @if(auth()->check())
                      <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Help Center </a></li>
                      <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Order History</a></li>
                      <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Change Password</a></li>
                      <li class="pre-profile-1 pt-2 pb-2"><a href="#/" class="color" style="font-weight:600">Update Profile</a></li>
                      <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.logout') }}" class="color" style="font-weight:600">Log Out</a></li>
                    @else
                      <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.login') }}" class="color" style="font-weight:600">Log In </a></li>
                      <li class="pre-profile-1 pt-2 pb-2"><a href="{{ route('user.register') }}" class="color" style="font-weight:600">Register</a></li>
                    @endif
                  </ul>
                  <span class="menu-1"><a href="home" class="color">home</a></span>
                  <span class="menu"><a href="product" class="color">P and P</a></span>
                  <span class="menu"><a href="about_us" class="color">about</a></span>
                  <span class="menu"><a href="share_info" class="color">share info</a></span>
                  <span class="menu-bar text-right"><a href="#/" class="color">&#9776;</a></span>
                  <span class="menu-bar-1 text-right"><a href="#/" class="color">&#9776;</a></span>
                  <span class="menu-bar-2 text-right"><a href="#/" class="color">&#9776;</a></span>
                </div>
              </div>
              <div class="container-fluid side-bar px-0">
                <div class="col-12 text-right bg-danger"> 
                  <span class="close"><a href="#/" class="color">&times;</a></span>
                </div>
                  <ul class="pt-5 pre-side-bar">
                    <li class="pt-4" style="margin-left:-5%;"><a href="home" class="color">home</a></li>
                    <li class="pt-3"style="margin-left:-5%"><a href="product" class="color">P and P</a></li>
                    <li class="pt-3"style="margin-left:-5%"><a href="about_us" class="color">about</a></li>
                    <li class="pt-3"style="margin-left:-5%"><a href="share_info" class="color">share info</a></li>
                    <li class="pt-3"style="margin-left:-5%"><a href="#/" class="color">Update profile</a></li>
                    <li class="pt-3"style="margin-left:-5%"><a href="ecommerce.html" class="color">Log Out</a><span><a href="#" class="color"><img src="log-in..png" class="log-in"></a></span></li>
                  </ul>
              </div>
            </div>
            <div class="carousel-inner text-center">
              <div class="carousel-item " style="height:100vh"> 
                <div style="height:100vh;width:100vw" class="text-right pr-5">
                  <div class="container pt-4">
                    <div class="row mt-3 pt-5">
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item active" style="height:100vh">
                <div class="container pt-2 pt-5 pb-5">
                  <div class="row pt-5 pb-5">
                    <div class="col-12 text-center pb-5 pt-4">
                        <a><span class="hide-lady-1"><img src="{{asset('assets/Images/coba1.jpeg')}}" height="350px" style="padding-left:6%;margin-top:3%;"></span></a>
                        <a><span class="hide-lady-1"><img src="{{asset('assets/Images/coba2.jpeg')}}" height="350px" style="padding-left:6%;margin-top:3%"></span></a>
                        <a><span class="hide-lady-1"><img src="{{asset('assets/Images/coba3.jpeg')}}" height="350px" style="padding-left:6%;margin-top:3%"></span></a>
                    </div>
                </div>
              </div>
          </div>
        </div>
          <div class='container-fluid pb-3'>
            <div class="row">
              <div class="col text-center">
                <a><span style="font-size:30px;text-transform:uppercase;font-weight:bold" data-aos="zoom-in">popular</span></a>
              </div>
            </div>
          </div>
          <div class="container-fluid pt-5" style="background-color:white">
            <div class="container"> 
              <div class="row text-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 text-center">
                  <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/coba4.jpeg')}}" class="img-men" data-aos="fade-up" style="border-radius:20px;"><br></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Simbar Menjangan</span></a>
                  </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 text-center popular-sift-down">
                  <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/coba5.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"><br></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Karet Kebo</span></a>
                  </div>
                </div>
                <div class="col-xl-3 col-lg-4 hide-lady-1">
                  <div class="container pl-0 text-center" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/coba6.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Janda Bolong</span></a>
                  </div>
                </div>
                <div class="col-xl-3 hide-lady">
                  <div class="container pl-0 text-center" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/coba7.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"></a>
                    <a><span class="image-lable pl-3" data-aos="fade-up"><br>Lili Paris</span></a>
                  </div>
                </div>
              </div>
              <div class="row pt-5 pb-4 text-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 text-center">
                  <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/hama3.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"><br></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Hama Ulat Bulu</span></a>
                  </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 text-center popular-sift-down">
                    <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/hama4.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"><br></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Hama Belalang</span></a>
                  </div>
                </div>
                <div class="col-xl-3 col-lg-4 text-center hide-lady-1">
                  <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/hama6.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Hama Tikus</span></a>
                  </div>
                </div>
                <div class="col-xl-3 text-center hide-lady">
                  <div class="container pl-0" height="250px" width="250px" style="overflow:hidden">
                    <a><img src="{{asset('assets/Images/hama5.jpeg')}}" class="img-men" style="border-radius:20px;" data-aos="fade-up"></a>
                    <a><span class="image-lable" data-aos="fade-up"><br>Hama Lalat</span></a>
                  </div>
                </div>
              </div>
              <div class="row mb-1 pb-3">
                <div class="col-12 pb-5 text-center">
                  <a href="product"><span style="font-weight:700;font-size:20px;color:blue" data-aos="zoom-in">see more</span><span class="pl-3" style="color:blue"><i class="fa fa-arrow-right" aria-hidden="true" data-aos="zoom-in"></i></span></a>
                </div>
              </div>
            </div>
          </div>
          <div class="container-fluid transparent pt-5 pb-5" id="parallax1" style="background-image:url('assets/Images/rumah.jpeg');background-size:cover;background-attachment:fixed;">
            <div class="container sift-couple" height="200px">
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 text-center">
                  <img src="{{asset('assets/Images/about us.jpeg')}}" height="230px" class="pl-5" data-aos="fade-up">
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 pt-4 text-center"> 
                  <span style="font-weight:700;font-size:20px;" data-aos="zoom-in"> Web ini menyediakan sarana edukasi mengenai tanaman hias. Mulai dari bagaimana cara merawat
                    dan membasmi hama - hama yang dapat merusak tanaman.
                    Web ini dibuat user friendly agar memudahkan pengguna dalam menggunakan web.
                    Kami memiliki fitur utama yaitu P AND P. Kami juga merekomendasikan obat - obat untuk membasmi hama - hama bukan hanya
                    hama tanaman tetapi juga hama rumah.</span><br><br><a href="product"><span style="font-size:20px;color:rgb(180, 69, 69);font-weight: 800;text-transform: uppercase;" data-aos="zoom-in">Pilih tanaman yang ingin anda ketahui</span></a>
                </div>
              </div>
            </div>
          </div>
          <div class="container mt-5"> 
            <div class="row">
              <div class="col-sm-12 text-center pb-4 pt-4">
             
              </div>
            </div>
            <div class="carousel slide pt-1" data-ride="carousel">
              <div class="carousel-inner">
                
                                                         <!--footer-->
          <div class="container-fluid bg-dark text-white">
            <div class="container">
            <div class="row ">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5">
                <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-right">Roro Jonggrang</p>
                <p data-aos="slide-right">Web Rorojonggrang<br>
                dibuat sejak tahun 2021.<br>
                Web tentang tanaman hias dan,<br> 
                hama serta menjual produk pembasmi hama.<br></p>
              </div>
              <div class="col-xl-3 pt-5 hide-girl">
                <span style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="zoom-in-left">P and P</span><br>
                <p style="margin-top:3%" data-aos="slide-up">Tanaman Hias</p>
                <p style="margin-top:-3%" data-aos="slide-up">Jenis Hewan</p>
                <p style="margin-top:-3%" data-aos="slide-up">Pembasmi Hama</p>
             </div>
              <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 footer  pt-5">
                <span style="font-size:20px;text-transform: uppercase;font-weight: 800;" class="text-center" data-aos="zoom-in-left">Social Media </span><br>
                <p style="margin-top:3%;margin-left:1%;"data-aos="fade-up">@noratrivetisia</p>
                <p style="margin-top:-3%;margin-left:1%;"data-aos="fade-up">@Triwulandari</p>
                <p style="margin-top:-3%;margin-left:1%;"data-aos="fade-up">@NawangAnggita</p>
              </div>
              <div class="col-xl-2 col-lg-4 col-md-4 col-sm-12 col-12 footer pt-5 pb-3">
                <p style="font-size:20px;text-transform: uppercase;font-weight: 800;" data-aos="slide-left">Contact</p>
                <p data-aos="fade-up"><i class="fa fa-home" aria-hidden="true" style="font-size:20px;"></i>&nbsp; Purwokerto, 53147,</p>
                <p data-aos="fade-up"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; rorojonggrang@gmail.com</p>
                <p data-aos="fade-up"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +62 876 9875 0123</p>
              </div>
            </div>
          </div>
          <hr style="color:white;width:100%;border:1px solid">
          <div class="container mt-4 pb-4">
            <div class="row">
              <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 ">
                <p>© 2020 Copyright:<span style="font-weight:600">Rorojonggrang</span></p>
              </div>
              <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-6 icon-head">
                <span class="icons-2"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                <span class="icons-3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                <span class="icons-1"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                <span class="icons"><i class="fa fa-instagram" aria-hidden="true"></i></span>
              </div>
            </div>
          </div>
          </div>
          <!--footer-->
          <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
          <script>
            AOS.init({
              once:true,
              duration:1000,
            });
          /* const parallax1 =document.getElementById('parallax1');
          const parallax2 =document.getElementById('parallax2');
          window.addEventListener("scroll",function(){
            let offset = window.pageYOffset;
            parallax1.style.backgroundPositionY = offset * 0.4 + 'px';
            parallax2.style.backgroundPositionY = offset * 0.9 + 'px';
          });*/
        $('.history').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            infinite:false,
            autoplaySpeed: 2000,
            }); 
            /*$(window).width > 991){
              alert("hello user")
            }
            else{ 
              $(".show").css("display","none")
            }
         
            /*$( window ).resize(function() { 
              if ($(window).innerWidth() <= 991){
                $('.history').slick({
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  autoplay: true,
                  autoplaySpeed: 2000,
                });
              }else{
                $('.history').slick({
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  autoplay: true,
                  autoplaySpeed: 2000,
                });
              }
            });*/
           
        </script>
    </body>
</html>