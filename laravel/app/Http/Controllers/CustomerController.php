<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use File;

class CustomerController extends Controller
{
    public function home()
    {
        return view('home');
    }

    
    public function product()
    {   
        return view('product');
    }
    public function share_info()
    {   
        return view('share_info');
    }
    public function about_us()
    {
        return view('about_us');
    }
    public function kutu_putih()
    {
        return view('hama.kutu_putih');
    }
    public function login_form()
    {
        return view('login_form');
    }
    public function ulat_bulu()
    {
        return view('hama.ulat_bulu');
    }
    public function wareng_coklat()
    {
        return view('hama.wareng_coklat');
    }
    public function fungus_gnats()
    {
        return view('hama.fungus_gnats');
    }
    public function simbar_menjangan()
    {
        return view('tumbuhan.simbar_menjangan');
    }
    public function kebo_karet()
    {
        return view('tumbuhan.kebo_karet');
    }
    public function janda_bolong()
    {
        return view('tumbuhan.janda_bolong');
    }
    public function lili_paris()
    {
        return view('tumbuhan.lili_paris');
    }
    public function baygon()
    {
        return view('produk.baygon');
    }
    public function insektisida()
    {
        return view('produk.insektisida');
    }
    public function cklt()
    {
        return view('produk.cklt');
    }
    public function ralat()
    {
        return view('produk.ralat');
    }
    public function cart()
    {
        return view('cart');
    }
}
