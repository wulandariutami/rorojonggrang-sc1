<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class AuthController extends Controller
{
    public function login(){
        return view('login_form');
    }

    public function authenticate(Request $request) {
        $validate = $request->validate([
            'username'=>['required'],
            'password'=>['required']
        ]);

        if (Auth::attempt($validate)) {
            $request->session()->regenerate();

            return redirect()->route('customer.home');
        }

        return back()->withErrors([
            'username' => 'The provided credentials do not match our records.',
        ]);
    }

    public function register() {
        return view('login_form');
    }

    public function store(Request $request) {
        $validate = $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'email'=>['required', 'unique:users'],
            'password'=>['required', 'confirmed']
        ]);
        
        User::create([
            'name'=>$validate['name'],
            'username'=>$validate['username'],
            'email'=>$validate['email'],
            'password'=>Hash::make($validate['password']),
            'is_admin'=>0
        ]);

        return redirect()->route('home');
    }

    public function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('customer.home');
    }   
}
