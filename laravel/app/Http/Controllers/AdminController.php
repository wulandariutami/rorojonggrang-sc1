<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use File;

use App\Models\User;
use App\Models\Tanaman;
use App\Models\Hama;
use App\Models\Product;

class AdminController extends Controller
{
    public function index(){
        return view('admin.index',[
            'module_name'=>'Dashboard',
            'user_count'=>User::count(),
            'tanaman_count'=>Tanaman::count(),
            'hama_count'=>Hama::count(),
            'product_count'=>Product::count(),
        ]);
    }

    // User
    public function user_index() {
        return view('admin.user-index',[
            'module_name'=>'User',
            'users'=>User::all()
        ]);
    }

    public function user_create() {
        return view('admin.user-create',[
            'module_name'=>'User',
            'submodule_name'=>'Tambah Data'
        ]);
    }

    public function user_store(Request $request) {
        $validate = $request->validate([
            'name'=>['required'],
            'username'=>['required'],
            'email'=>['required', 'unique:users'],
            'password'=>['required', 'confirmed'],
            'is_admin'=>['required', 'in:0,1']
        ]);

        User::create([
            'name'=>$validate['name'],
            'username'=>$validate['username'],
            'email'=>$validate['email'],
            'password'=>Hash::make($validate['password']),
            'is_admin'=>$validate['is_admin']
        ]);

        return redirect()->route('admin.user.index');
    }

    public function user_edit(User $user) {
        return view('admin.user-create',[
            'module_name'=>'User',
            'submodule_name'=>'Edit Data',
            'user'=>$user
        ]);
    }

    public function user_update(Request $request, User $user) {

    }

    public function user_destroy(User $user) {
        
    }

    // tanaman
    public function tanaman_index() {
        return view('admin.tanaman-index',[
            'module_name'=>'Tanaman',
            'tanamans'=>Tanaman::all()
        ]);
    }

    public function tanaman_create() {
        return view('admin.tanaman-create',[
            'module_name'=>'Tanaman',
            'submodule_name'=>'Tambah Data'
        ]);
    }
    
    public function tanaman_store(Request $request) {
        $validate = $request->validate([
            'name'=>['required'],
            'jenis'=>['required'],
            'desc'=>['required'],
            'image'=>['required', 'file', 'mimes:jpg,png,jpeg'],
            'link'=>['required']
        ]);

        if($request->hasFile('image')) {
            $extFile = $validate['image']->getClientOriginalExtension();
            $namaFile = 'tanaman-'.time().'.'.$extFile;
            $path = $request->image->move('assets/images/tanaman/'.$namaFile);
        }

        Tanaman::create([
            'name'=>$validate['name'],
            'jenis'=>$validate['jenis'],
            'desc'=>$validate['desc'],
            'image'=>$path,
            'link'=>$validate['link'],
        ]);

        return redirect()->route('admin.tanaman.index');
    }

    public function tanaman_edit(Tanaman $tanaman) {
        return view('admin.tanaman-edit',[
            'module_name'=>'Tanaman',
            'submodule_name'=>'Edit Data',
            'tanaman'=>$tanaman
        ]);
    }

    public function tanaman_update(Request $request, Tanaman $tanaman) {

        if($request->hasFile('image')) {
            File::delete($tanaman->image);
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'tanaman-'.time().'.'.$extFile;
            $path = $request->image->move('assets/images/tanaman/'.$namaFile);
            $tanaman->image = $path;
        }

        $tanaman->name = $request->name;
        $tanaman->jenis = $request->jenis;
        $tanaman->desc = $request->desc;
        $tanaman->link = $request->link;
        $tanaman->save();

        return redirect()->route('admin.tanaman.index');
    }

    public function tanaman_destroy(Tanaman $tanaman) {
        File::delete('assets/images/tanaman/'.$tanaman->image);
        $tanaman->delete();
        return redirect()->route('admin.tanaman.index');
    }

    // hama
    public function hama_index() {
        return view('admin.hama-index',[
            'module_name'=>'Hama',
        ]);
    }

    public function hama_create() {
        return view('admin.hama-create',[
            'module_name'=>'Hama',
            'submodule_name'=>'Tambah Data'
        ]);
    }
    
    public function hama_store(Request $request) {
    }

    public function hama_edit(Hama $hama) {
        return view('admin.hama-edit',[
            'module_name'=>'User',
            'submodule_name'=>'Edit Data',
        ]);
    }

    public function hama_update(Request $request, Hama $hama) {

    }

    public function hama_destroy(Hama $hama) {
        
    }

    // produk
    public function product_index() {
        return view('admin.product-index',[
            'module_name'=>'Product',
        ]);
    }

    public function product_create() {
        return view('admin.product-create',[
            'module_name'=>'Product',
            'submodule_name'=>'Tambah Data'
        ]);
    }

    public function product_store(Request $request) {
    }

    public function product_edit(Product $product) {
        return view('admin.product-edit',[
            'module_name'=>'Product',
            'submodule_name'=>'Edit Data',
        ]);
    }

    public function product_update(Request $request, Product $product) {

    }

    public function product_destroy(Product $product) {
        
    }
}
