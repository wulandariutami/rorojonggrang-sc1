<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'user', 'as'=>'user.'], function() {
    Route::group(['middleware'=>'guest'], function() {
        Route::get('/register', [AuthController::class, 'register'])->name('register');
        Route::post('/store', [AuthController::class, 'store'])->name('store');
        Route::get('/login', [AuthController::class, 'login'])->name('login');
        Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('auth');
    });

    Route::get('/logout', [AuthController::class, 'logout'])->name('logout')
    ->middleware('auth');
});

Route::group(['as'=>'customer.'], function() {
    Route::get('/home', [CustomerController::class, 'home'])->name('home');
    Route::get('/product', [CustomerController::class, 'product'])->name('product');
    Route::get('/about_us', [CustomerController::class, 'about_us'])->name('about_us');
    Route::get('/share_info', [CustomerController::class, 'share_info'])->name('share_info');
});

Route::group(['prefix'=>'admin', 'as'=>'admin.', 'middleware'=>'auth'], function() {
    Route::get('/', [AdminController::class, 'index'])->name('index');

    Route::group(['prefix'=>'user', 'as'=>'user.'], function() {
        Route::get('/', [AdminController::class, 'user_index'])->name('index');
        Route::get('/create', [AdminController::class, 'user_create'])->name('create');
        Route::post('/store', [AdminController::class, 'user_store'])->name('store');
        Route::get('/{user}/edit', [AdminController::class, 'user_edit'])->name('edit');
        Route::put('/{user}/update', [AdminController::class, 'user_update'])->name('update');
        Route::delete('/{user}', [AdminController::class, 'user_destroy'])->name('destroy');
    });
    Route::group(['prefix'=>'tanaman', 'as'=>'tanaman.'], function() {
        Route::get('/', [AdminController::class, 'tanaman_index'])->name('index');
        Route::get('/create', [AdminController::class, 'tanaman_create'])->name('create');
        Route::post('/store', [AdminController::class, 'tanaman_store'])->name('store');
        Route::get('/{tanaman}/edit', [AdminController::class, 'tanaman_edit'])->name('edit');
        Route::put('/{tanaman}/update', [AdminController::class, 'tanaman_update'])->name('update');
        Route::delete('/{tanaman}', [AdminController::class, 'tanaman_destroy'])->name('destroy');
    });
    Route::group(['prefix'=>'hama', 'as'=>'hama.'], function() {
        Route::get('/', [AdminController::class, 'hama_index'])->name('index');
        Route::get('/create', [AdminController::class, 'hama_create'])->name('create');
        Route::post('/store', [AdminController::class, 'hama_store'])->name('store');
        Route::get('/{hama}/edit', [AdminController::class, 'hama_edit'])->name('edit');
        Route::put('/{hama}/update', [AdminController::class, 'hama_update'])->name('update');
        Route::delete('/{hama}', [AdminController::class, 'hama_destroy'])->name('destroy');
    });
    Route::group(['prefix'=>'product', 'as'=>'product.'], function() {
        Route::get('/', [AdminController::class, 'product_index'])->name('index');
        Route::get('/create', [AdminController::class, 'product_create'])->name('create');
        Route::post('/store', [AdminController::class, 'product_store'])->name('store');
        Route::get('/{product}/edit', [AdminController::class, 'product_edit'])->name('edit');
        Route::put('/{product}/update', [AdminController::class, 'product_update'])->name('update');
        Route::delete('/{product}', [AdminController::class, 'product_destroy'])->name('destroy');
    });
});

Route::get('index', [AdminLTEController::class, 'index'])->name('index');

Route::group(['prefix'=>'page', 'as'=>'page.'], function() {
    Route::get('/kutu_putih', [CustomerController::class, 'kutu_putih'])->name('kutu_putih');
    Route::get('/ulat_bulu', [CustomerController::class, 'ulat_bulu'])->name('ulat_bulu');
    Route::get('/wareng_coklat', [CustomerController::class, 'wareng_coklat'])->name('wareng_coklat');
    Route::get('/fungus_gnats', [CustomerController::class, 'fungus_gnats'])->name('fungus_gnats');
    Route::get('/simbar_menjangan', [CustomerController::class, 'simbar_menjangan'])->name('simbar_menjangan');
    Route::get('/lili_paris', [CustomerController::class, 'lili_paris'])->name('lili_paris');
    Route::get('/janda_bolong', [CustomerController::class, 'janda_bolong'])->name('janda_bolong');
    Route::get('/kebo_karet', [CustomerController::class, 'kebo_karet'])->name('kebo_karet');
    Route::get('/baygon', [CustomerController::class, 'baygon'])->name('baygon');
    Route::get('/insektisida', [CustomerController::class, 'insektisida'])->name('insektisida');
    Route::get('/cklt', [CustomerController::class, 'cklt'])->name('cklt');
    Route::get('/ralat', [CustomerController::class, 'ralat'])->name('ralat');
    Route::get('/cart', [CustomerController::class, 'cart'])->name('cart');

    Route::get('/admin-index', function () {
        // dd(url('')."/laravel/vendor");
        return view('admin.index');
    });
});
